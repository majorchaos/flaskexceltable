
import sys
sys.path.insert(0, '/var/www/flaskexceltable')

python_home = '/var/www/flaskexceltable/venv'

activate_this = python_home + '/bin/activate_this.py'
with open(activate_this) as file_:
  exec(file_.read(), dict(__file__=activate_this))

from app import app as application


